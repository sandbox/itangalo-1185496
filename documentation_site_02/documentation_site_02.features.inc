<?php
/**
 * @file
 * documentation_site_02.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function documentation_site_02_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function documentation_site_02_node_info() {
  $items = array(
    'docscollection' => array(
      'name' => t('Documentation collection'),
      'base' => 'node_content',
      'description' => t('A documentation collection referes to one or several documentation pages on the site, making it easier to navigate them. Only you can edit your own documentation collections.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'docspage' => array(
      'name' => t('Documentation page'),
      'base' => 'node_content',
      'description' => t('Documentation pages are editable by all site members, and contain documentation about a given concept.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
