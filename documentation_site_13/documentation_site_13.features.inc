<?php
/**
 * @file
 * documentation_site_13.features.inc
 */

/**
 * Implementation of hook_views_api().
 */
function documentation_site_13_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => 3.0);
  }
}

/**
 * Implementation of hook_flag_default_flags().
 */
function documentation_site_13_flag_default_flags() {
  $flags = array();
  // Exported flag: "Favorite documentation".
  $flags['favorite'] = array(
    'content_type' => 'node',
    'title' => 'Favorite documentation',
    'global' => '0',
    'types' => array(
      0 => 'docspage',
    ),
    'flag_short' => 'Mark [node:title] as favorite',
    'flag_long' => 'Click here to have this documentation appear in your favorite list.',
    'flag_message' => '[node:title] is now in your favorite list',
    'unflag_short' => 'Remove [node:title] from your favorite list',
    'unflag_long' => 'Click here to remove this documentation from your favorite list.',
    'unflag_message' => '[node:title] has been removed from your favorite list',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'api_version' => 2,
    'module' => 'documentation_site_13',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;
}
