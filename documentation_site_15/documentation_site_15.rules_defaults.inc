<?php
/**
 * @file
 * documentation_site_15.rules_defaults.inc
 */

/**
 * Implementation of hook_default_rules_configuration().
 */
function documentation_site_15_default_rules_configuration() {
  $items = array();
  $items['1'] = entity_import('rules_config', '{ "rules_comment_notification" : {
      "LABEL" : "Comment notification",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "comment" ],
      "ON" : [ "comment_insert" ],
      "IF" : [
        { "NOT data_is" : { "data" : [ "comment:author" ], "value" : [ "comment:node:author" ] } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "comment:node:author:mail" ],
            "subject" : "New comment to [comment:node:title]",
            "message" : "A new comment has been posted to your [comment:node:type] [comment:node:title].\\r\\nYou can view the comment at [comment:url].\\r\\n\\r\\nKind regards,\\r\\n\\/\\/The robot at [site:name]"
          }
        },
        { "drupal_message" : { "message" : "Message sent to [comment:node:author:mail]:\\u003cbr\\/\\u003e\\r\\n\\u003cbr\\/\\u003e\\r\\nA new comment has been posted to your [comment:node:type] [comment:node:title].\\u003cbr\\/\\u003e\\r\\nYou can view the comment at [comment:url].\\u003cbr\\/\\u003e\\r\\n\\u003cbr\\/\\u003e\\r\\nKind regards,\\u003cbr\\/\\u003e\\r\\n\\/\\/The robot at [site:name]" } }
      ]
    }
  }');
  return $items;
}
