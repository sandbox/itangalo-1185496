<?php
/**
 * @file
 * documentation_site_01_documentation_page.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function documentation_site_01_documentation_page_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function documentation_site_01_documentation_page_node_info() {
  $items = array(
    'docspage' => array(
      'name' => t('Documentation page'),
      'base' => 'node_content',
      'description' => t('Documentation pages are editable by all site members, and contain docmentation about a given concept.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
