<?php
/**
 * @file
 * documentation_site_08.features.inc
 */

/**
 * Implementation of hook_views_api().
 */
function documentation_site_08_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => 3.0);
  }
}
