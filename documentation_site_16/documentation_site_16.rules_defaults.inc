<?php
/**
 * @file
 * documentation_site_16.rules_defaults.inc
 */

/**
 * Implementation of hook_default_rules_configuration().
 */
function documentation_site_16_default_rules_configuration() {
  $items = array();
  $items['5'] = entity_import('rules_config', '{ "rules_schedule_reminder" : {
      "LABEL" : "Schedule reminder",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "rules_scheduler" ],
      "ON" : [ "user_login" ],
      "DO" : [
        { "schedule" : {
            "component" : "rules_send_reminder_email",
            "date" : "+1 month",
            "identifier" : "Send reminder to to user [account:uid].",
            "param_account" : [ "account" ]
          }
        }
      ]
    }
  }');
  return $items;
}
