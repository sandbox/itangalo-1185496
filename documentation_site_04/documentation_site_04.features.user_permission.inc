<?php
/**
 * @file
 * documentation_site_04.features.user_permission.inc
 */

/**
 * Implementation of hook_user_default_permissions().
 */
function documentation_site_04_user_default_permissions() {
  $permissions = array();

  // Exported permission: access user profiles
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'user',
  );

  // Exported permission: create docscollection content
  $permissions['create docscollection content'] = array(
    'name' => 'create docscollection content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create docspage content
  $permissions['create docspage content'] = array(
    'name' => 'create docspage content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any docscollection content
  $permissions['delete any docscollection content'] = array(
    'name' => 'delete any docscollection content',
    'roles' => array(),
  );

  // Exported permission: delete any docspage content
  $permissions['delete any docspage content'] = array(
    'name' => 'delete any docspage content',
    'roles' => array(),
  );

  // Exported permission: delete own docscollection content
  $permissions['delete own docscollection content'] = array(
    'name' => 'delete own docscollection content',
    'roles' => array(),
  );

  // Exported permission: delete own docspage content
  $permissions['delete own docspage content'] = array(
    'name' => 'delete own docspage content',
    'roles' => array(),
  );

  // Exported permission: edit any docscollection content
  $permissions['edit any docscollection content'] = array(
    'name' => 'edit any docscollection content',
    'roles' => array(),
  );

  // Exported permission: edit any docspage content
  $permissions['edit any docspage content'] = array(
    'name' => 'edit any docspage content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own docscollection content
  $permissions['edit own docscollection content'] = array(
    'name' => 'edit own docscollection content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own docspage content
  $permissions['edit own docspage content'] = array(
    'name' => 'edit own docspage content',
    'roles' => array(),
  );

  return $permissions;
}
