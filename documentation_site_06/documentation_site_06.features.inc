<?php
/**
 * @file
 * documentation_site_06.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function documentation_site_06_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_image_default_styles().
 */
function documentation_site_06_image_default_styles() {
  $styles = array();

  // Exported image style: retro
  $styles['retro'] = array(
    'name' => 'retro',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '16',
          'height' => '16',
        ),
        'weight' => '1',
      ),
      2 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => '240',
          'height' => '240',
          'upscale' => 1,
        ),
        'weight' => '2',
      ),
    ),
  );

  return $styles;
}

/**
 * Implementation of hook_node_info().
 */
function documentation_site_06_node_info() {
  $items = array(
    'docscollection' => array(
      'name' => t('Documentation collection'),
      'base' => 'node_content',
      'description' => t('A documentation collection referes to one or several documentation pages on the site, making it easier to navigate them. Only you can edit your own documentation collections.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'docspage' => array(
      'name' => t('Documentation page'),
      'base' => 'node_content',
      'description' => t('Documentation pages are editable by all site members, and contain documentation about a given concept.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
