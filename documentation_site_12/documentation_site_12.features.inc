<?php
/**
 * @file
 * documentation_site_12.features.inc
 */

/**
 * Implementation of hook_views_api().
 */
function documentation_site_12_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => 3.0);
  }
}

/**
 * Implementation of hook_flag_default_flags().
 */
function documentation_site_12_flag_default_flags() {
  $flags = array();
  // Exported flag: "Friend flag".
  $flags['friend'] = array(
    'content_type' => 'user',
    'title' => 'Friend flag',
    'global' => '0',
    'types' => array(),
    'flag_short' => 'Mark [user:name] as your friend',
    'flag_long' => 'Click here to have this user listed in your friends tab.',
    'flag_message' => '[user:name] is now flagged as a friend',
    'unflag_short' => 'Remove [user:name] from your friend list',
    'unflag_long' => 'Click here to remove this user from your friends tab.',
    'unflag_message' => '[user:name] is no longer listed on your friends tab.',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'show_on_profile' => 1,
    'access_uid' => 'others',
    'api_version' => 2,
    'module' => 'documentation_site_12',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;
}
