<?php
/**
 * @file
 * documentation_site_20.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function documentation_site_20_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_views_api().
 */
function documentation_site_20_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => 3.0);
  }
}
